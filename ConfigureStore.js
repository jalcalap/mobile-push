import { createStore, applyMiddleware, combineReducers } from 'redux'
import customApiMiddleware from './middleware'
import rootReducer from './reducers'
import { apiMiddleware } from 'redux-api-middleware';
import thunk from 'redux-thunk';
import api from 'redux-cached-api-middleware';


export default function configureSore() {
  // const composeEnhancers =
  //   window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  return createStore(
    rootReducer, {},
    applyMiddleware(thunk, apiMiddleware, customApiMiddleware() )
      //  composeEnhancers(applyMiddleware(apiMiddleware)//, routerMiddleware(history)))
    );
}

