/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { connect } from "react-redux";
import { View } from "react-native";
import PropTypes from "prop-types";
import AppNavigator from "./src/navigators/AppNavigator";
import LoginStack from "./src/navigators/stacks/LoginStack";
import FirebaseNotifications from "./firebase-notifications";

// GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

// const config = {
//   url: 'https://keycloak.cern.ch/auth',
//   realm: 'master',
//   clientId: 'push-notifications',
//   redirectUri: 'http://pushnotifications-mobile-auth.web.cern.ch/',
// };

// Login.startLoginProcess(config).then(tokens => {
// });
class App extends Component {
  async componentDidMount() {
    FirebaseNotifications.checkPermission();
    FirebaseNotifications.createNotificationListeners();
  }

  componentWillUnmount() {
    FirebaseNotifications.notificationListener();
    FirebaseNotifications.notificationOpenedListener();
  }

  render() {
    const { loggedIn } = this.props;
    return (
      <View style={{ flex: 1 }}>
        {loggedIn ? <AppNavigator /> : <LoginStack />}
      </View>
    );
  }
}

App.propTypes = {
  loggedIn: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  return {
    loggedIn: state.auth.loggedIn
  };
};

export default connect(mapStateToProps)(App);
