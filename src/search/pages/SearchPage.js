import React, { Component } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { SearchBar } from "react-native-elements";
import { TabView, SceneMap } from "react-native-tab-view";
import PropTypes from "prop-types";
import NotificationsList from "../../notifications/components/NotificationsList";
import ServicesList from "../../services/components/ServicesList";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});

export default class SearchPage extends Component {
  state = {
    search: "",
    index: 0,
    routes: [
      { key: "services", title: "Services" },
      { key: "notifications", title: "Notifications" }
    ]
  };

  componentDidMount() {
    const { findAllServices, findAllNotifications } = this.props;
    console.log("que co;o pasa aqui")
    findAllServices();
    findAllNotifications();
  }

  updateSearch = search => {
    this.setState({ search });
  };

  filterNotifications(notifications) {
    let { search } = this.state;
    search = search.toUpperCase();
    return notifications.filter(n => {
      return (
        n.from.name.toUpperCase().includes(search) ||
        n.subject.toUpperCase().includes(search) ||
        n.body.toUpperCase().includes(search)
      );
    });
  }

  filteredNotifications() {
    const {
      navigation,
      notifications,
      findAllNotifications,
      updateNotification,
      clearCache
    } = this.props;
    return (
      <NotificationsList
        updateNotification={updateNotification}
        findAllNotifications={findAllNotifications}
        clearCache={clearCache}
        notifications={this.filterNotifications(notifications)}
        navigation={navigation}
      />
    );
  }

  filteredServices() {
    const {
      navigation,
      notifications,
      updateNotification,
      services
    } = this.props;
    let { search } = this.state;
    search = search.toUpperCase();
    return (
      <ServicesList
        navigation={navigation}
        notifications={notifications}
        updateNotification={updateNotification}
        services={services.filter(s => s.name.toUpperCase().includes(search))}
      />
    );
  }

  render() {
    const { search } = this.state;

    return (
      <View style={styles.container}>
        <SearchBar
          placeholder="Type Here..."
          round
          showLoadingIcon
          onChangeText={this.updateSearch}
          value={search}
          containerStyle={{
            backgroundColor: "#2196F3",
            borderBottomColor: "transparent",
            borderTopColor: "transparent"
          }}
          inputContainerStyle={{ backgroundColor: "white" }}
          inputStyle={{ backgroundColor: "white" }}
        />
        <TabView
          navigationState={this.state}
          renderScene={SceneMap({
            services: () => this.filteredServices(),
            notifications: () => this.filteredNotifications()
          })}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{
            width: Dimensions.get("window").width,
            height: 0
          }}
        />
      </View>
    );
  }
}

SearchPage.propTypes = {
  findAllServices: PropTypes.func.isRequired,
  findAllNotifications: PropTypes.func.isRequired,
  notifications: PropTypes.arrayOf(Object).isRequired,
  updateNotification: PropTypes.func.isRequired,
  services: PropTypes.arrayOf(Object).isRequired,
  clearCache: PropTypes.func.isRequired
};
