import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";
import { withRefresh } from "../utils/authUtils";

export const LOGOUT = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

export const logout = () => ({
  [RSAA]: {
    endpoint: `${BASE_URL}/logout`,
    method: "POST",
    credentials: "include",
    headers: withRefresh({ "Content-Type": "application/json" }),
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAILURE]
  }
});
