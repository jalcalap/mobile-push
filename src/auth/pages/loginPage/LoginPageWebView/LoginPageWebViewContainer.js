import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { login } from "../../../actions/Login";
import LoginPageWebView from "./LoginPageWebView";
import { registerDeviceToken } from "../../../../notifications/actions/RegisterDeviceToken";

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      login,
      registerDeviceToken
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPageWebView);
