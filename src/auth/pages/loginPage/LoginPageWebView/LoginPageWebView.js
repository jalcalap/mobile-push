import React, { Component } from "react";
import { WebView } from "react-native-webview";
import PropTypes from "prop-types";
import firebase from "react-native-firebase";

import {
  OAUTH_REDIRECT_URI,
  OAUTH_CLIENT_ID,
  OAUTH_AUTHORIZE_PATH
} from "react-native-dotenv";

export default class OAuthObtainCode extends Component {
  static getParameterByName(url, name) {
    const parameterName = name.replace(/[[\]]/g, "\\$&");
    const regex = new RegExp(`[?&]${parameterName}(=([^&#]*)|&|#|$)`);
    const results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  onNavigationStateChange = webViewState => {
    const { login, registerDeviceToken } = this.props;
    console.debug("WebView current url", webViewState.url);
    if (webViewState.url.startsWith(OAUTH_REDIRECT_URI)) {
      const codeUrlParam = OAuthObtainCode.getParameterByName(
        webViewState.url,
        "code"
      );
      if (codeUrlParam) {
        console.debug("CERN OAuth code:", codeUrlParam);
        login(codeUrlParam).then(async () => {
          const fcmToken = await firebase.messaging().getToken();
          registerDeviceToken(fcmToken);
        });
      }
    }
  };

  render() {
    const url = `${OAUTH_AUTHORIZE_PATH}?redirect_uri=${OAUTH_REDIRECT_URI}&client_id=${OAUTH_CLIENT_ID}&response_type=code`;
    console.debug("WebView loading:", url);
    return (
      <WebView
        source={{ uri: url }}
        startInLoadingState
        onNavigationStateChange={this.onNavigationStateChange}
        javaScriptEnabled // TODO to be tested on Android
      />
    );
  }
}

OAuthObtainCode.propTypes = {
  login: PropTypes.func.isRequired,
  registerDeviceToken: PropTypes.func.isRequired
};
