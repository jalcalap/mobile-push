import { AsyncStorage } from "react-native";
import jwtDecode from "jwt-decode";
import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE } from "../actions/Login";
import { LOGOUT_SUCCESS, LOGOUT_FAILURE } from "../actions/Logout";
import {
  REFRESH_TOKEN,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILURE
} from "../actions/RefreshToken";

const initialState = {
  loggedIn: false,
  loginInProgress: false,
  username: undefined,
  access: undefined,
  refresh: undefined,
  error: undefined
};

const checkUnauthorizedAction = action => {
  if (
    typeof action.payload === "object" &&
    action.payload.toString().includes("401 - Unauthorized")
  ) {
    AsyncStorage.removeItem("accessToken");
  }
};

/**
 * Reducer function for the austhentication actions
 *
 * @param state Authentication state
 * @param action
 * @returns {{refresh: boolean, loggedIn: boolean, loginInProgress: boolean, errors: {}}}
 */
export default (state = initialState, action) => {
  checkUnauthorizedAction(action);

  switch (action.type) {
    case LOGIN:
    case REFRESH_TOKEN:
      return {
        ...state,
        loginInProgress: true
      };
    case LOGIN_SUCCESS:
    case REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        loginInProgress: false,
        loggedIn: true,
        access: action.payload.accessToken,
        refresh: action.payload.refreshToken,
        username: jwtDecode(action.payload.accessToken).username,
        errors: {}
      };
    case LOGIN_FAILURE:
    case REFRESH_TOKEN_FAILURE:
    case LOGOUT_FAILURE:
      return {
        ...state,
        loggedIn: false,
        loginInProgress: false,
        access: undefined,
        refresh: undefined,
        username: undefined,
        errors: action.payload.response || {
          non_field_errors: action.payload.statusText
        }
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        loggedIn: false,
        loginInProgress: false,
        access: undefined,
        refresh: undefined,
        username: undefined
      };
    default:
      return {
        ...state
      };
  }
};
