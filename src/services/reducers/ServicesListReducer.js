import {
  GET_SERVICES,
  GET_SERVICES_SUCCESS,
  GET_SERVICES_FAILURE
} from "../actions/FindAllServices";

const INITIAL_STATE = {
  services: [],
  error: null,
  loading: false
};

function processGetNotifications(state) {
  return {
    ...state,
    loading: true
  };
}

function processGetNotificationsSuccess(state, services) {
  return {
    services,
    error: null,
    loading: false
  };
}

function processGetNotificationsFailure(state, error) {
  return {
    ...state,
    services: [],
    error,
    loading: false
  };
}

export default function(state = INITIAL_STATE, action) {
  let error;

  switch (action.type) {
    case GET_SERVICES:
      return processGetNotifications(state);
    case GET_SERVICES_SUCCESS:
      return processGetNotificationsSuccess(state, action.payload);
    case GET_SERVICES_FAILURE:
      error = action.payload || { message: action.payload.message };
      return processGetNotificationsFailure(state, error);

    default:
      return state;
  }
}
