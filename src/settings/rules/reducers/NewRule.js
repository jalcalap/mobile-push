import {
  CREATE_RULE,
  CREATE_RULE_SUCCESS,
  CREATE_RULE_FAILURE
} from "../actions/CreateRule";

const INITIAL_STATE = {
  rule: null,
  error: null,
  loading: false
};

function processCreateRule(state) {
  return {
    ...state,
    loading: true
  };
}

function processCreateRuleSuccess(state, notification) {
  return {
    ...state,
    notification,
    error: null,
    loading: false
  };
}

function processCreateRuleFailure(state, error) {
  return {
    ...state,
    notification: null,
    error,
    loading: false
  };
}

export default function(state = INITIAL_STATE, action) {
  let error;

  switch (action.type) {
    case CREATE_RULE:
      return processCreateRule(state);
    case CREATE_RULE_SUCCESS:
      return processCreateRuleSuccess(state, action.payload);
    case CREATE_RULE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return processCreateRuleFailure(state, error);

    default:
      return state;
  }
}
