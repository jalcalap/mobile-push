import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";
import { withAuth } from "../../../auth/utils/authUtils";

// Create new notification
export const GET_RULES = "GET_RULES";
export const GET_RULES_SUCCESS = "GET_RULES_SUCCESS";
export const GET_RULES_FAILURE = "GET_RULES_FAILURE";

export const findAllRules = () => {
  return {
    [RSAA]: {
      endpoint: `${BASE_URL}/rules`,
      method: "GET",
      credentials: "include",
      headers: withAuth({ "Content-Type": "application/json" }),
      types: [GET_RULES, GET_RULES_SUCCESS, GET_RULES_FAILURE]
    }
  };
};
