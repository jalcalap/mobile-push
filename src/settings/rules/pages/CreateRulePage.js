import { View, Text, StyleSheet } from "react-native";
import React, { Component } from "react";
import { TextField } from "react-native-material-textfield";
import { Dropdown } from "react-native-material-dropdown";
import { RadioButton } from "react-native-paper";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "white"
  },
  radioBtn: {
    flexDirection: "row",
    alignItems: "center"
  }
});

class CreateRulePage extends Component {
  types = [
    {
      value: "All by email or push",
      type: "allEmailOrPush"
    },
    {
      value: "From contains...",
      type: "fromContains"
    }
  ];

  state = {
    name: "",
    ruleString: "",
    channel: "",
    type: ""
  };

  constructor(props) {
    super(props);
    this.onClickSave = this.save.bind(this, "onClickSave");
    const { navigation } = this.props;

    navigation.setParams({ onClickSave: this.onClickSave });
  }

  save() {
    const { name, ruleString, channel, type } = this.state;
    const { createRule, navigation } = this.props;
    createRule(
      {
        name,
        string: ruleString,
        channel
      },
      type
    );

    navigation.goBack();
  }

  render() {
    const { name, type, value } = this.state;
    const { channel } = this.state;
    return (
      <View style={styles.container}>
        <TextField
          label="Rule name"
          value={name}
          onChangeText={newName => this.setState({ name: newName })}
        />
        <Dropdown
          value={value}
          label="Type"
          data={this.types}
          onChangeText={(newValue, index) =>
            this.setState({ type: this.types[index].type })
          }
        />
        {type === "fromContains" && (
          <TextField
            label="String"
            value={this.ruleString}
            onChangeText={ruleString => this.setState({ ruleString })}
          />
        )}
        <Text>Channel</Text>
        <RadioButton.Group
          onValueChange={newChannel => this.setState({ channel: newChannel })}
          value={channel}
        >
          <View style={styles.radioBtn}>
            <RadioButton value="PUSH" />
            <Text>Push</Text>
          </View>
          <View style={styles.radioBtn}>
            <RadioButton value="EMAIL" />
            <Text>Email</Text>
          </View>
        </RadioButton.Group>
      </View>
    );
  }
}

CreateRulePage.propTypes = {
  createRule: PropTypes.func.isRequired
};

export default CreateRulePage;
