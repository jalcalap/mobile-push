import React from "react";
import { View, StyleSheet } from "react-native";
import ActionButton from "react-native-action-button";
import RulesListContainer from "../containers/RulesListContainer";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});

const RulesPage = props => {
  const { navigation } = props;
  return (
    <View style={styles.container}>
      <RulesListContainer navigation={navigation} />
      <ActionButton
        buttonColor="#2185d0"
        onPress={() => navigation.navigate("CreateRulePage")}
      />
    </View>
  );
};

export default RulesPage;
