import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as findRulesActionsCreators from "../actions/FindAllRules";
import RulesList from "../components/RulesList";

const mapStateToProps = state => {
  return {
    rules: state.rules.rulesList.rules
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(findRulesActionsCreators, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RulesList);
