import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as createRulesActionsCreators from "../actions/CreateRule";
import CreateRulePage from "../pages/CreateRulePage";

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(createRulesActionsCreators, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateRulePage);
