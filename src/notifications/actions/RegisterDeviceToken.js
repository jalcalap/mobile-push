import { BASE_URL } from "react-native-dotenv";
import { RSAA } from "redux-api-middleware";
import { withAuth } from "../../auth/utils/authUtils";

export const REGISTER_DEVICE_TOKEN = "REGISTER_DEVICE_TOKEN";
export const REGISTER_DEVICE_TOKEN_SUCCESS = "REGISTER_DEVICE_TOKEN_SUCCESS";
export const REGISTER_DEVICE_TOKEN_FAILURE = "REGISTER_DEVICE_TOKEN_FAILURE";

export const registerDeviceToken = token => ({
  [RSAA]: {
    endpoint: `${BASE_URL}/users/token`,
    method: "POST",
    body: JSON.stringify({ token }),
    credentials: "include",
    headers: withAuth({ "Content-Type": "application/json" }),
    types: [
      REGISTER_DEVICE_TOKEN,
      REGISTER_DEVICE_TOKEN_SUCCESS,
      REGISTER_DEVICE_TOKEN_FAILURE
    ]
  }
});
