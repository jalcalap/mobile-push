import React, { Component } from "react";
import {
  RefreshControl,
  FlatList,
  TouchableOpacity,
  Text,
  View,
  StyleSheet
} from "react-native";
import PropTypes from "prop-types";

import Notification from "./Notification";
import Separator from "../../common/Separator";

const styles = StyleSheet.create({
  center: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  }
});

export default class NotificationsList extends Component {
  constructor(props) {
    super(props);
    this.update = this.update.bind(this);
  }

  onPress({ notification }) {
    const { notifications, navigation } = this.props;
    this.update({ ...notification, read: true });
    navigation.navigate("NotificationDetails", {
      update: this.update,
      notification,
      notifications
    });
  }

  onRefresh() {
    const { clearCache, findAllNotifications } = this.props;
    clearCache();
    findAllNotifications();
  }

  update(notification) {
    const { updateNotification } = this.props;
    updateNotification(notification);
  }

  render() {
    const { notifications, filterFunction, loading } = this.props;
    if (notifications.length === 0)
      return (
        <View style={styles.center}>
          <Text>You have not received any notification yet.</Text>
        </View>
      );

    return (
      <FlatList
        data={notifications
          .filter(filterFunction)
          .map((n, i) => ({ key: i.toString(), notification: n }))}
        ItemSeparatorComponent={Separator}
        style={{
          backgroundColor: "white"
        }}
        refreshControl={
          <RefreshControl
            colors={["#9Bd35A", "#689F38"]}
            refreshing={loading}
            onRefresh={() => this.onRefresh()}
          />
        }
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => this.onPress(item)}>
            <Notification
              {...this.props}
              key={item.key}
              notification={item.notification}
            />
          </TouchableOpacity>
        )}
      />
    );
  }
}

NotificationsList.defaultProps = {
  filterFunction: () => true,
  loading: false
};

NotificationsList.propTypes = {
  notifications: PropTypes.arrayOf(Object).isRequired,
  clearCache: PropTypes.func.isRequired,
  findAllNotifications: PropTypes.func.isRequired,
  filterFunction: PropTypes.func,
  loading: PropTypes.bool,
  updateNotification: PropTypes.func.isRequired
};
