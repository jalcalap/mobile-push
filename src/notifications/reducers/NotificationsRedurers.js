import { combineReducers } from "redux";
import notificationsList from "./NotificationsListReducer";

const notificationsReducer = combineReducers({
  notificationsList
});

export default notificationsReducer;
