import { connect } from "react-redux";
import api from "redux-cached-api-middleware";
import NotificationsPage from "../../pages/NotificationsPage";
import {
  findAllNotifications,
  NOTIFICATIONS_KEY
} from "../../actions/FindAllNotifications";

const mapStatetoProps = state => {
  return {
    loading: state.notifications.notificationsList.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    findAllNotifications: () => dispatch(findAllNotifications),
    clearCache: () =>
      dispatch(
        api.actions.clearCache({
          cacheKey: NOTIFICATIONS_KEY
        })
      )
  };
};

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(NotificationsPage);
