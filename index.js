import React from "react";
import { Provider } from "react-redux";
import { AppRegistry } from "react-native";
import { NativeRouter } from "react-router-native";
import { persistStore } from "redux-persist";
import { Sentry } from "react-native-sentry";
import App from "./App";
import { name as appName } from "./app.json";
import configureStore from "./ConfigureStore";

const store = configureStore();
// Sentry.config(
//   "https://a1f17677df154e14862dba7fefa4390b:6612d18c95ba4d6aa04afe84bda161e7@dev-push-notifications-sentry.web.cern.ch/2"
// ).install();
persistStore(store);

const CERNnotifications = () => {
  return (
    <Provider store={store}>
      <NativeRouter>
        <App />
      </NativeRouter>
    </Provider>
  );
};

AppRegistry.registerComponent(appName, () => CERNnotifications);
