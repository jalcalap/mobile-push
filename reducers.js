import { combineReducers } from "redux"
import AuthReducer from "./src/auth/reducers/index";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rulesReducers from "./src/settings/rules/reducers/RulesReducers";
import notificationsReducer from "./src/notifications/reducers/NotificationsRedurers";
import servicesReducer from "./src/services/reducers/ServicesReducers";
import FilesystemStorage from 'redux-persist-filesystem-storage'
import api from 'redux-cached-api-middleware';


const authPersistConfig = {
  key: 'auth',
  storage: storage
};


export default combineReducers({
  auth: persistReducer(authPersistConfig, AuthReducer),
  rules: rulesReducers,
  notifications: notificationsReducer,
  services: servicesReducer,
  [api.constants.NAME]: api.reducer,
});

// export default combineReducers({
//   auth: AuthReducer
// });
